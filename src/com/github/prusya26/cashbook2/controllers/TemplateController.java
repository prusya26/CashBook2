package com.github.prusya26.cashbook2.controllers;

import com.github.prusya26.cashbook2.common.FXUtils;
import com.github.prusya26.cashbook2.controllers.model.CashDayDetailTemplateObservable;
import com.github.prusya26.cashbook2.data.ConnectionSourceFactory;
import com.github.prusya26.cashbook2.data.DaoFactory;
import com.github.prusya26.cashbook2.data.ModelFactory;
import com.github.prusya26.cashbook2.data.model.CashDayDetailTemplate;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;

import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.concurrent.Callable;

public class TemplateController implements Initializable {
    public Button addBtn;
    public Button delBtn;
    public TableView<CashDayDetailTemplateObservable> detailTable;
    public TableColumn<CashDayDetailTemplateObservable, String> docNumCol;
    public TableColumn<CashDayDetailTemplateObservable, String> docInfoCol;
    public TableColumn<CashDayDetailTemplateObservable, String> accNumCol;
    public Button saveBtn;
    public Button cancelBtn;
    public ProgressIndicator progressIndicator;

    private List<CashDayDetailTemplate> deletedList;

    public void handleToolButtonAction(ActionEvent actionEvent) {
        if(actionEvent.getSource().equals(addBtn)) {
            CashDayDetailTemplateObservable newRow = ModelFactory.createEmptyCashDayDetailTemplateObservable();
            detailTable.getItems().add(newRow);
            detailTable.getSelectionModel().select(detailTable.getItems().size() - 1, docNumCol);
            detailTable.edit(detailTable.getItems().size() - 1, docNumCol);
        } else if(actionEvent.getSource().equals(delBtn)) {
            if(detailTable.getSelectionModel().getSelectedItem() != null) {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Cash Book 2");
                alert.setHeaderText("Удаление строки шаблона");
                alert.setContentText("Вы действительно хотите удалить эту строку?");

                Optional<ButtonType> optional = alert.showAndWait();
                if(optional.isPresent() && optional.get() == ButtonType.OK) {
                    detailTable.getItems().remove(detailTable.getSelectionModel().getSelectedIndex());
                }
            }
        }
    }

    public void onButtonAction(ActionEvent actionEvent) {
        if(actionEvent.getSource().equals(saveBtn)) {
            try {
                TransactionManager.callInTransaction(ConnectionSourceFactory.getConnectionSource(), new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        Dao<CashDayDetailTemplate, Long> dao = DaoFactory.getCashDayDetailTemplateDao();
                        if(deletedList != null) dao.delete(deletedList);
                        for (CashDayDetailTemplateObservable i : detailTable.getItems()) {
                            dao.createOrUpdate(i.getSource());
                        }
                        return null;
                    }
                });
                ((Stage) saveBtn.getScene().getWindow()).close();
            } catch (Exception e) {
                Alert alert = FXUtils.createExceptionDialog("Cash Book 2", "Ошибка сохранения шаблона в базу данных", null, e);
                alert.showAndWait();
            }
        } else if(actionEvent.getSource().equals(cancelBtn)) {
            ((Stage) cancelBtn.getScene().getWindow()).close();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        docNumCol.setCellValueFactory(param -> param.getValue().documentNumberProperty());
        docNumCol.setCellFactory(TextFieldTableCell.forTableColumn());

        docInfoCol.setCellValueFactory(param -> param.getValue().documentDescriptionProperty());
        docInfoCol.setCellFactory(TextFieldTableCell.forTableColumn());

        accNumCol.setCellValueFactory(param -> param.getValue().accountNumberProperty());
        accNumCol.setCellFactory(TextFieldTableCell.forTableColumn());

        LoadDataTask task = new LoadDataTask();
        task.setOnFailed(event -> {
            progressIndicator.setVisible(false);
            Alert alert = FXUtils.createExceptionDialog("Cash Book 2", "Ошибка получения строк шаблона из базы данных", null, event.getSource().getException());
            alert.showAndWait();
        });
        task.setOnSucceeded(event -> {
            detailTable.setItems(task.getValue());
            detailTable.getItems().addListener((ListChangeListener<CashDayDetailTemplateObservable>) c -> {
                while (c.next()) {
                    if (c.wasRemoved()) {
                        if (deletedList == null) deletedList = new LinkedList<>();
                        c.getRemoved().forEach(o -> {
                            if (o.getId() != 0L) {
                                deletedList.add(o.getSource());
                            }
                        });
                    }
                }
            });
            progressIndicator.setVisible(false);
        });
        Thread thread = new Thread(task);
        thread.start();
    }

    private class LoadDataTask extends Task<ObservableList<CashDayDetailTemplateObservable>> {
        @Override
        protected ObservableList<CashDayDetailTemplateObservable> call() throws Exception {
            return ModelFactory.getCashDayDetailTemplateObservableList();
        }
    }
}
