package com.github.prusya26.cashbook2.controllers;

import com.github.prusya26.cashbook2.common.AppProperties;
import com.github.prusya26.cashbook2.common.BigDecimalStringConverter;
import com.github.prusya26.cashbook2.common.FXUtils;
import com.github.prusya26.cashbook2.controllers.model.CashDayDetailObservable;
import com.github.prusya26.cashbook2.controllers.model.CashDayObservable;
import com.github.prusya26.cashbook2.data.ConnectionSourceFactory;
import com.github.prusya26.cashbook2.data.DaoFactory;
import com.github.prusya26.cashbook2.data.ModelFactory;
import com.github.prusya26.cashbook2.data.model.CashDay;
import com.github.prusya26.cashbook2.data.model.CashDayDetail;
import com.github.prusya26.cashbook2.data.model.CashDayDetailTemplate;
import com.ibm.icu.text.RuleBasedNumberFormat;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.stmt.QueryBuilder;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;

import java.math.BigDecimal;
import java.net.URL;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.concurrent.Callable;

public class DetailWindowController implements Initializable {
    private CashDayObservable cashDay;

    //Поля окна
    public ProgressIndicator progressIndicator;
    public DatePicker dateEdt;
    public TextField inRestEdt;
    public Button addBtn;
    public Button delBtn;
    public TableView<CashDayDetailObservable> detailTable;
    public TableColumn<CashDayDetailObservable, String> docNumCol;
    public TableColumn<CashDayDetailObservable, String> docInfoCol;
    public TableColumn<CashDayDetailObservable, String> accNumCol;
    public TableColumn<CashDayDetailObservable, BigDecimal> prihodCol;
    public TableColumn<CashDayDetailObservable, BigDecimal> rashodCol;
    public TextField outRestEdt;
    public Spinner<Integer> listSpinner;
    public TextField prihodColEdt;
    public TextField rashodColEdt;
    public Button okBtn;
    public Button cancelBtn;

    private int threadFinishedCount = 0;
    private int threadFinishedMaxCount = 0;
    private boolean okPressed;

    private RuleBasedNumberFormat numberFormat;
    private DecimalFormat format;
    private Date originalDate;

    public DetailWindowController() {
        format = (DecimalFormat) NumberFormat.getNumberInstance(Locale.forLanguageTag("ru"));
        format.setParseBigDecimal(true);
        numberFormat = new RuleBasedNumberFormat(Locale.forLanguageTag("ru"), RuleBasedNumberFormat.SPELLOUT);
    }

    public void handleToolButtonAction(ActionEvent actionEvent) {
        if(actionEvent.getSource().equals(addBtn)) {
            CashDayDetailObservable newRow = ModelFactory.createEmptyCashDayDetailObservable();
            newRow.setCashDay(cashDay.getSource());
            setRecalcListenersToTableRow(newRow);
            detailTable.getItems().add(newRow);
            detailTable.getSelectionModel().select(detailTable.getItems().size() - 1, docNumCol);
            detailTable.edit(detailTable.getItems().size() - 1, docNumCol);
        } else if(actionEvent.getSource().equals(delBtn)) {
            if(detailTable.getSelectionModel().getSelectedItem() != null) {
                detailTable.getItems().remove(detailTable.getSelectionModel().getSelectedIndex());
            }
        }
    }

    public void handleButtonAction(ActionEvent actionEvent) {
        if(actionEvent.getSource().equals(okBtn)) {
            try {
                TransactionManager.callInTransaction(ConnectionSourceFactory.getConnectionSource(), new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        Dao<CashDay, Date> dayDao = DaoFactory.getCashDayDao();
                        dayDao.createOrUpdate(cashDay.getSource());
                        Dao<CashDayDetail, Long> detailDao = DaoFactory.getCashDayDetailDao();
                        for(CashDayDetailObservable r : detailTable.getItems()) {
                            detailDao.createOrUpdate(r.getSource());
                        }
                        if(originalDate != null && !originalDate.equals(cashDay.getDate())) {
                            dayDao.deleteById(originalDate);
                        }
                        return null;
                    }
                });
                okPressed = true;
                ((Stage) okBtn.getScene().getWindow()).close();
            } catch (SQLException e) {
                Alert alert = FXUtils.createExceptionDialog("Cash Book 2", "Ошибка сохранения данных", null, e);
                alert.showAndWait();
            }
        } else if(actionEvent.getSource().equals(cancelBtn)) {
            okPressed = false;
            ((Stage) cancelBtn.getScene().getWindow()).close();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        detailTable.setItems(FXCollections.observableArrayList());
        detailTable.getSelectionModel().setCellSelectionEnabled(true);

        docNumCol.setCellValueFactory(param -> param.getValue().documentNumberProperty());
        docNumCol.setCellFactory(TextFieldTableCell.forTableColumn());
        docNumCol.setOnEditCommit(event -> {
            event.getRowValue().setDocumentNumber(event.getNewValue());
            event.getTableView().requestFocus();
        });

        docInfoCol.setCellValueFactory(param -> param.getValue().documentDescriptionProperty());
        docInfoCol.setCellFactory(TextFieldTableCell.forTableColumn());
        docInfoCol.setOnEditCommit(event -> {
            event.getRowValue().setDocumentDescription(event.getNewValue());
            event.getTableView().requestFocus();
        });

        accNumCol.setCellValueFactory(param -> param.getValue().accountNumberProperty());
        accNumCol.setCellFactory(TextFieldTableCell.forTableColumn());
        accNumCol.setOnEditCommit(event -> {
            event.getRowValue().setAccountNumber(event.getNewValue());
            event.getTableView().requestFocus();
        });

        prihodCol.setCellValueFactory(param -> param.getValue().prihodProperty());
        prihodCol.setCellFactory(TextFieldTableCell.forTableColumn(new BigDecimalStringConverter()));
        prihodCol.setOnEditCommit(event -> {
            event.getRowValue().setPrihod(event.getNewValue());
            event.getTableView().requestFocus();
        });

        rashodCol.setCellValueFactory(param -> param.getValue().rashodProperty());
        rashodCol.setCellFactory(TextFieldTableCell.forTableColumn(new BigDecimalStringConverter()));
        rashodCol.setOnEditCommit(event -> {
            event.getRowValue().setRashod(event.getNewValue());
            event.getTableView().requestFocus();
        });

        //Перерасчеты
        inRestEdt.textProperty().addListener((observable, oldValue, newValue) -> recalc());
        detailTable.getItems().addListener((ListChangeListener<CashDayDetailObservable>) c -> recalc());
        dateEdt.valueProperty().addListener((observable, oldValue, newValue) -> {
            cashDay.setDate(Date.from(newValue.atStartOfDay(ZoneId.systemDefault()).toInstant()));
            detailTable.getItems().forEach(r -> r.setCashDay(cashDay.getSource()));
        });
        listSpinner.valueProperty().addListener((observable, oldValue, newValue) -> cashDay.setListNumber(newValue));
        inRestEdt.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                cashDay.setInRest((BigDecimal) format.parse(newValue.replace('.', ',')));
            } catch (ParseException ignored) {
            }
        });
        outRestEdt.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                cashDay.setOutRest((BigDecimal) format.parse(newValue.replace('.', ',')));
            } catch (ParseException ignored) {
            }
        });
        prihodColEdt.textProperty().addListener((observable, oldValue, newValue) -> cashDay.setPrihodCount(newValue));
        rashodColEdt.textProperty().addListener((observable, oldValue, newValue) -> cashDay.setRashodCount(newValue));
    }

    private void recalc() {
        try {
            BigDecimal inRest = (BigDecimal) format.parse(inRestEdt.getText());
            BigDecimal prihodSum = BigDecimal.ZERO;
            BigDecimal rashodSum = BigDecimal.ZERO;
            int prihodCount = 0;
            int rashodCount = 0;
            for (CashDayDetailObservable r : detailTable.getItems()) {
                if(r.getPrihod() != null && !BigDecimal.ZERO.equals(r.getPrihod())) {
                    prihodSum = prihodSum.add(r.getPrihod());
                    prihodCount++;
                }
                if(r.getRashod() != null && !BigDecimal.ZERO.equals(r.getRashod())) {
                    rashodSum = rashodSum.add(r.getRashod());
                    rashodCount++;
                }
            }
            BigDecimal outRest = inRest.add(prihodSum).subtract(rashodSum);
            outRestEdt.setText(format.format(outRest));
            prihodColEdt.setText(numberFormat.format(prihodCount));
            rashodColEdt.setText(numberFormat.format(rashodCount));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public CashDayObservable getCashDay() {
        return cashDay;
    }

    private void hideProgressIndicatorIfNeeded() {
        threadFinishedCount++;
        if(threadFinishedCount >= threadFinishedMaxCount) {
            progressIndicator.setVisible(false);
        }
    }

    private void createNewDayFromPrev() {
        cashDay.setDate(new Date());
        LoadPrevCashDateTask loadPrevCashDateTask = new LoadPrevCashDateTask(cashDay.getDate());
        loadPrevCashDateTask.setOnFailed(event -> {
            hideProgressIndicatorIfNeeded();
            Alert alert = FXUtils.createExceptionDialog("Cash Book 2", "Ошибка получения входящего остатка", null, event.getSource().getException());
            alert.showAndWait();
        });
        loadPrevCashDateTask.setOnSucceeded(event -> {
            hideProgressIndicatorIfNeeded();
            CashDay prevCashDay = loadPrevCashDateTask.getValue();
            if(prevCashDay != null) {
                cashDay.setInRest(prevCashDay.getOutRest());
                cashDay.setOutRest(prevCashDay.getOutRest());
                cashDay.setListNumber(prevCashDay.getListNumber());
                loadDayDataToControls();
            }
        });
        Thread thread = new Thread(loadPrevCashDateTask);
        thread.start();
    }

    private void loadDayDataToControls() {
        dateEdt.setValue(cashDay.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
        try {
            inRestEdt.setText(format.format(cashDay.getInRest()));
            outRestEdt.setText(format.format(cashDay.getOutRest()));
        } catch (Exception ignored) {}
        listSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 9999999, cashDay.getListNumber()));
        prihodColEdt.setText(cashDay.getPrihodCount());
        rashodColEdt.setText(cashDay.getRashodCount());
    }

    private void loadDetailsFromFilledDay() {
        LoadPrevCashDateDetailsTask loadTask = new LoadPrevCashDateDetailsTask(cashDay.getDate());
        loadTask.setOnFailed(event -> {
            hideProgressIndicatorIfNeeded();
            Alert alert = FXUtils.createExceptionDialog("Cash Book 2", "Ошибка получения детальных записей из предыдущего заполненного дня", null, event.getSource().getException());
            alert.showAndWait();
        });
        loadTask.setOnSucceeded(event -> {
            hideProgressIndicatorIfNeeded();
            List<CashDayDetail> cashDayDetails = loadTask.getValue();
            if(cashDayDetails != null) {
                cashDayDetails.forEach(prevDetail -> {
                    CashDayDetailObservable detail = ModelFactory.createEmptyCashDayDetailObservable();
                    detail.setCashDay(cashDay.getSource());
                    detail.setDocumentNumber(prevDetail.getDocumentNumber());
                    detail.setAccountNumber(prevDetail.getAccountNumber());
                    detail.setDocumentDescription(prevDetail.getDocumentDescription());
                    detailTable.getItems().add(detail);
                });
                setRecalcListenersToTableRows();
            }
        });
        Thread t = new Thread(loadTask);
        t.start();
    }

    private void loadDetailsFromTemplate() {
        LoadTemplateTask loadTask = new LoadTemplateTask();
        loadTask.setOnFailed(event -> {
            hideProgressIndicatorIfNeeded();
            Alert alert = FXUtils.createExceptionDialog("Cash Book 2", "Ошибка получения детальных записей из шаблона", null, event.getSource().getException());
            alert.showAndWait();
        });
        loadTask.setOnSucceeded(event -> {
            hideProgressIndicatorIfNeeded();
            List<CashDayDetailTemplate> templateList = loadTask.getValue();
            templateList.forEach(t -> {
                CashDayDetailObservable detail = ModelFactory.createEmptyCashDayDetailObservable();
                detail.setCashDay(cashDay.getSource());
                detail.setDocumentNumber(t.getDocumentNumber());
                detail.setAccountNumber(t.getAccountNumber());
                detail.setDocumentDescription(t.getDocumentDescription());
                detailTable.getItems().add(detail);
            });
            setRecalcListenersToTableRows();
        });
        Thread t = new Thread(loadTask);
        t.start();
    }

    private void loadDetails() {
        LoadDetailsTask loadDetailsTask = new LoadDetailsTask(cashDay.getDate());
        loadDetailsTask.setOnFailed(event -> {
            hideProgressIndicatorIfNeeded();
            Alert alert = FXUtils.createExceptionDialog("Cash Book 2", "Ошибка получения детальных записей", null, event.getSource().getException());
            alert.showAndWait();
        });
        loadDetailsTask.setOnSucceeded(event -> {
            hideProgressIndicatorIfNeeded();
            detailTable.getItems().addAll(loadDetailsTask.getValue());
            setRecalcListenersToTableRows();
        });
        Thread t = new Thread(loadDetailsTask);
        t.start();
    }

    public void setCashDay(CashDayObservable cashDay) {
        this.cashDay = cashDay;
        //Если дата null - значит это новый объект
        if(cashDay.getDate() == null) {
            threadFinishedMaxCount = 2;
            createNewDayFromPrev();
            if(AppProperties.get().getDetailFillStrategy() == AppProperties.DetailFillStrategy.LastFilledDay) {
                loadDetailsFromFilledDay();
            } else {
                loadDetailsFromTemplate();
            }
        } else {
            originalDate = cashDay.getDate();
            threadFinishedMaxCount = 1;
            loadDayDataToControls();
            loadDetails();
        }
    }

    private void setRecalcListenersToTableRows() {
        detailTable.getItems().forEach(this::setRecalcListenersToTableRow);
    }

    private void setRecalcListenersToTableRow(CashDayDetailObservable row) {
        row.prihodProperty().addListener((observable, oldValue, newValue) -> recalc());
        row.rashodProperty().addListener((observable, oldValue, newValue) -> recalc());
    }

    public boolean isOkPressed() {
        return okPressed;
    }

    public void setOkPressed(boolean okPressed) {
        this.okPressed = okPressed;
    }

    private class LoadPrevCashDateTask extends Task<CashDay> {
        private final Date startDate;

        public LoadPrevCashDateTask(Date startDate) {
            this.startDate = startDate;
        }

        @Override
        protected CashDay call() throws Exception {
            Dao<CashDay, Date> dao = DaoFactory.getCashDayDao();
            QueryBuilder<CashDay, Date> builder = dao.queryBuilder();
            builder.where().lt(CashDay.Columns.CASH_DATE, startDate);
            builder.orderBy(CashDay.Columns.CASH_DATE, false);
            return dao.queryForFirst(builder.prepare());
        }
    }

    private class LoadPrevCashDateDetailsTask extends Task<List<CashDayDetail>> {
        private final Date startDate;

        public LoadPrevCashDateDetailsTask(Date startDate) {
            this.startDate = startDate;
        }

        @Override
        protected List<CashDayDetail> call() throws Exception {
            Dao<CashDayDetail, Long> dao = DaoFactory.getCashDayDetailDao();
            QueryBuilder<CashDayDetail, Long> builder = dao.queryBuilder();
            builder.where().lt(CashDayDetail.Columns.CASH_DATE, startDate);
            builder.orderBy(CashDayDetail.Columns.CASH_DATE, false);
            CashDayDetail first = dao.queryForFirst(builder.prepare());
            if(first != null) {
                Date prevDateNotEmpty = first.getCashDay().getDate();
                builder = dao.queryBuilder();
                builder.where().eq(CashDayDetail.Columns.CASH_DATE, prevDateNotEmpty);
                return dao.query(builder.prepare());
            } else {
                return null;
            }
        }
    }

    private class LoadTemplateTask extends Task<List<CashDayDetailTemplate>> {
        @Override
        protected List<CashDayDetailTemplate> call() throws Exception {
            Dao<CashDayDetailTemplate, Long> dao = DaoFactory.getCashDayDetailTemplateDao();
            return dao.queryForAll();
        }
    }

    private class LoadDetailsTask extends Task<ObservableList<CashDayDetailObservable>> {
        private final Date date;

        public LoadDetailsTask(Date date) {
            this.date = date;
        }

        @Override
        protected ObservableList<CashDayDetailObservable> call() throws Exception {
            Dao<CashDayDetail, Long> dao = DaoFactory.getCashDayDetailDao();
            List<CashDayDetail> list = dao.queryForEq(CashDayDetail.Columns.CASH_DATE, date);
            ObservableList<CashDayDetailObservable> result = FXCollections.observableArrayList();
            list.forEach(i -> result.add(new CashDayDetailObservable(i)));
            return result;
        }
    }

//    private class BigDecimalStringConverter extends StringConverter<BigDecimal> {
//        private DecimalFormat format;
//
//        public BigDecimalStringConverter() {
//            format = (DecimalFormat) NumberFormat.getNumberInstance(Locale.getDefault());
//            format.setParseBigDecimal(true);
//        }
//
//        @Override
//        public String toString(BigDecimal object) {
//            return format.format(object);
//        }
//
//        @Override
//        public BigDecimal fromString(String string) {
//            try {
//                return (BigDecimal) format.parse(string);
//            } catch (ParseException e) {
//                return null;
//            }
//        }
//    }
}
