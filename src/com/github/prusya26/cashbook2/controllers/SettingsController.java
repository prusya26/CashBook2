package com.github.prusya26.cashbook2.controllers;

import com.github.prusya26.cashbook2.common.AppProperties;
import com.github.prusya26.cashbook2.common.FXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class SettingsController implements Initializable {
    public Button saveBtn;
    public Button cancelBtn;
    public TextField dateFormatEdt;
    public ToggleGroup fillingGroup;
    public Button showTemplatesBtn;
    public RadioButton lastFilledDayRB;
    public RadioButton templateRB;
    public TextField cashierEdt;

    public void onButtonActionHandler(ActionEvent actionEvent) {
        if(actionEvent.getSource().equals(saveBtn)) {
            //сохранение
            AppProperties properties = AppProperties.get();
            properties.setDateFormat(dateFormatEdt.getText());
            properties.setCashier(cashierEdt.getText());
            if(lastFilledDayRB.equals(fillingGroup.getSelectedToggle())) {
                properties.setDetailFillStrategy(AppProperties.DetailFillStrategy.LastFilledDay);
            } else {
                properties.setDetailFillStrategy(AppProperties.DetailFillStrategy.Template);
            }
            try {
                properties.save();
                ((Stage) saveBtn.getScene().getWindow()).close();
            } catch (IOException e) {
                Alert alert = FXUtils.createExceptionDialog("Cash Book 2", "Ошибка сохранения настроек", "Проверьте доступность каталога для записи", e);
                alert.showAndWait();
            }
        } else if(actionEvent.getSource().equals(cancelBtn)) {
            //отмена
            ((Stage) cancelBtn.getScene().getWindow()).close();
        } else if(actionEvent.getSource().equals(showTemplatesBtn)) {
            try {
                Parent root = FXMLLoader.load(getClass().getResource("/com/github/prusya26/cashbook2/fxml/template_window.fxml"));
                Stage stage = new Stage();
                stage.setTitle("Шаблон заполнения деталей дня");
                stage.setScene(new Scene(root));
                stage.initOwner(showTemplatesBtn.getScene().getWindow());
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.showAndWait();
            } catch (IOException e) {
                Alert alert = FXUtils.createExceptionDialog("Cash Book 2", "Ошибка открытия окна настроек шаблона деталей дня.", null, e);
                alert.showAndWait();
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        AppProperties properties = AppProperties.get();
        dateFormatEdt.setText(properties.getDateFormat());
        cashierEdt.setText(properties.getCashier());
        AppProperties.DetailFillStrategy strategy = properties.getDetailFillStrategy();
        if(strategy == AppProperties.DetailFillStrategy.LastFilledDay) {
            fillingGroup.selectToggle(lastFilledDayRB);
        } else {
            fillingGroup.selectToggle(templateRB);
        }
        showTemplatesBtn.disableProperty().bind(lastFilledDayRB.selectedProperty());
    }
}
