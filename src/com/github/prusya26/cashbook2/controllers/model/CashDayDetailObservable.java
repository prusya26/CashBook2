package com.github.prusya26.cashbook2.controllers.model;

import com.github.prusya26.cashbook2.data.model.CashDay;
import com.github.prusya26.cashbook2.data.model.CashDayDetail;
import javafx.beans.property.*;

import java.math.BigDecimal;

public class CashDayDetailObservable {
    private final CashDayDetail source;

    private LongProperty id;
    private ObjectProperty<CashDay> cashDay;
    private StringProperty documentNumber;
    private StringProperty documentDescription;
    private StringProperty accountNumber;
    private ObjectProperty<BigDecimal> prihod;
    private ObjectProperty<BigDecimal> rashod;

    public CashDayDetailObservable(CashDayDetail source) {
        this.source = source;
        id = new SimpleLongProperty(source.getId());
        id.addListener((observable, oldValue, newValue) -> source.setId(newValue.longValue()));
        cashDay = new SimpleObjectProperty<>(source.getCashDay());
        cashDay.addListener((observable, oldValue, newValue) -> source.setCashDay(newValue));
        documentNumber = new SimpleStringProperty(source.getDocumentNumber());
        documentNumber.addListener((observable, oldValue, newValue) -> source.setDocumentNumber(newValue));
        documentDescription = new SimpleStringProperty(source.getDocumentDescription());
        documentDescription.addListener((observable, oldValue, newValue) -> source.setDocumentDescription(newValue));
        accountNumber = new SimpleStringProperty(source.getAccountNumber());
        accountNumber.addListener((observable, oldValue, newValue) -> source.setAccountNumber(newValue));
        prihod = new SimpleObjectProperty<>(source.getPrihod());
        prihod.addListener((observable, oldValue, newValue) -> source.setPrihod(newValue));
        rashod = new SimpleObjectProperty<>(source.getRashod());
        rashod.addListener((observable, oldValue, newValue) -> source.setRashod(newValue));
    }

    public CashDayDetail getSource() {
        return source;
    }

    public long getId() {
        return id.get();
    }

    public LongProperty idProperty() {
        return id;
    }

    public void setId(long id) {
        this.id.set(id);
    }

    public CashDay getCashDay() {
        return cashDay.get();
    }

    public ObjectProperty<CashDay> cashDayProperty() {
        return cashDay;
    }

    public void setCashDay(CashDay cashDay) {
        this.cashDay.set(cashDay);
    }

    public String getDocumentNumber() {
        return documentNumber.get();
    }

    public StringProperty documentNumberProperty() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber.set(documentNumber);
    }

    public String getDocumentDescription() {
        return documentDescription.get();
    }

    public StringProperty documentDescriptionProperty() {
        return documentDescription;
    }

    public void setDocumentDescription(String documentDescription) {
        this.documentDescription.set(documentDescription);
    }

    public String getAccountNumber() {
        return accountNumber.get();
    }

    public StringProperty accountNumberProperty() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber.set(accountNumber);
    }

    public BigDecimal getPrihod() {
        return prihod.get();
    }

    public ObjectProperty<BigDecimal> prihodProperty() {
        return prihod;
    }

    public void setPrihod(BigDecimal prihod) {
        this.prihod.set(prihod);
    }

    public BigDecimal getRashod() {
        return rashod.get();
    }

    public ObjectProperty<BigDecimal> rashodProperty() {
        return rashod;
    }

    public void setRashod(BigDecimal rashod) {
        this.rashod.set(rashod);
    }
}
