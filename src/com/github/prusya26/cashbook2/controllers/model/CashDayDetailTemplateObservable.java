package com.github.prusya26.cashbook2.controllers.model;

import com.github.prusya26.cashbook2.data.model.CashDayDetailTemplate;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class CashDayDetailTemplateObservable {
    private final CashDayDetailTemplate source;

    private final LongProperty id;
    private final StringProperty documentNumber;
    private final StringProperty documentDescription;
    private final StringProperty accountNumber;

    public CashDayDetailTemplateObservable(CashDayDetailTemplate source) {
        this.source = source;

        id = new SimpleLongProperty(source.getId());
        id.addListener((observable, oldValue, newValue) -> source.setId(newValue.longValue()));
        documentNumber = new SimpleStringProperty(source.getDocumentNumber());
        documentNumber.addListener((observable, oldValue, newValue) -> source.setDocumentNumber(newValue));
        documentDescription = new SimpleStringProperty(source.getDocumentDescription());
        documentDescription.addListener((observable, oldValue, newValue) -> source.setDocumentDescription(newValue));
        accountNumber = new SimpleStringProperty(source.getAccountNumber());
        accountNumber.addListener((observable, oldValue, newValue) -> source.setAccountNumber(newValue));
    }

    public CashDayDetailTemplate getSource() {
        return source;
    }

    public long getId() {
        return id.get();
    }

    public LongProperty idProperty() {
        return id;
    }

    public void setId(long id) {
        this.id.set(id);
    }

    public String getDocumentNumber() {
        return documentNumber.get();
    }

    public StringProperty documentNumberProperty() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber.set(documentNumber);
    }

    public String getDocumentDescription() {
        return documentDescription.get();
    }

    public StringProperty documentDescriptionProperty() {
        return documentDescription;
    }

    public void setDocumentDescription(String documentDescription) {
        this.documentDescription.set(documentDescription);
    }

    public String getAccountNumber() {
        return accountNumber.get();
    }

    public StringProperty accountNumberProperty() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber.set(accountNumber);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CashDayDetailTemplateObservable that = (CashDayDetailTemplateObservable) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public boolean isEmpty() {
         return (documentNumber.get() == null || documentNumber.get().isEmpty())
                && (documentDescription.get() == null || documentDescription.get().isEmpty())
                && (accountNumber.get() == null || accountNumber.get().isEmpty());
    }
}
