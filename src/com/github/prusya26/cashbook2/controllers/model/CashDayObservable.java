package com.github.prusya26.cashbook2.controllers.model;

import com.github.prusya26.cashbook2.data.model.CashDay;
import javafx.beans.property.*;

import java.math.BigDecimal;
import java.util.Date;

public class CashDayObservable {
    private final CashDay source;

    private ObjectProperty<Date> date;
    private ObjectProperty<BigDecimal> inRest;
    private ObjectProperty<BigDecimal> outRest;
    private IntegerProperty listNumber;
    private StringProperty prihodCount;
    private StringProperty rashodCount;

    public CashDayObservable(CashDay source) {
        this.source = source;

        date = new SimpleObjectProperty<>(source.getDate());
        date.addListener((observable, oldValue, newValue) -> source.setDate(newValue));
        inRest = new SimpleObjectProperty<>(source.getInRest());
        inRest.addListener((observable, oldValue, newValue) -> source.setInRest(newValue));
        outRest = new SimpleObjectProperty<>(source.getOutRest());
        outRest.addListener((observable, oldValue, newValue) -> source.setOutRest(newValue));
        listNumber = new SimpleIntegerProperty(source.getListNumber());
        listNumber.addListener((observable, oldValue, newValue) -> source.setListNumber(newValue.intValue()));
        prihodCount = new SimpleStringProperty(source.getPrihodCount());
        prihodCount.addListener((observable, oldValue, newValue) -> source.setPrihodCount(newValue));
        rashodCount = new SimpleStringProperty(source.getRashodCount());
        rashodCount.addListener((observable, oldValue, newValue) -> source.setRashodCount(newValue));
    }

    public CashDay getSource() {
        return source;
    }

    public Date getDate() {
        return date.get();
    }

    public ObjectProperty<Date> dateProperty() {
        return date;
    }

    public void setDate(Date date) {
        this.date.set(date);
    }

    public BigDecimal getInRest() {
        return inRest.get();
    }

    public ObjectProperty<BigDecimal> inRestProperty() {
        return inRest;
    }

    public void setInRest(BigDecimal inRest) {
        this.inRest.set(inRest);
    }

    public BigDecimal getOutRest() {
        return outRest.get();
    }

    public ObjectProperty<BigDecimal> outRestProperty() {
        return outRest;
    }

    public void setOutRest(BigDecimal outRest) {
        this.outRest.set(outRest);
    }

    public int getListNumber() {
        return listNumber.get();
    }

    public IntegerProperty listNumberProperty() {
        return listNumber;
    }

    public void setListNumber(int listNumber) {
        this.listNumber.set(listNumber);
    }

    public String getPrihodCount() {
        return prihodCount.get();
    }

    public StringProperty prihodCountProperty() {
        return prihodCount;
    }

    public void setPrihodCount(String prihodCount) {
        this.prihodCount.set(prihodCount);
    }

    public String getRashodCount() {
        return rashodCount.get();
    }

    public StringProperty rashodCountProperty() {
        return rashodCount;
    }

    public void setRashodCount(String rashodCount) {
        this.rashodCount.set(rashodCount);
    }
}
