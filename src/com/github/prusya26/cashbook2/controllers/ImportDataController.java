package com.github.prusya26.cashbook2.controllers;

import com.github.prusya26.cashbook2.common.FXUtils;
import com.github.prusya26.cashbook2.data.ConnectionSourceFactory;
import com.github.prusya26.cashbook2.data.DaoFactory;
import com.github.prusya26.cashbook2.data.model.CashDay;
import com.github.prusya26.cashbook2.data.model.CashDayDetail;
import com.github.prusya26.cashbook2.data.model.old.OldDetail;
import com.github.prusya26.cashbook2.data.model.old.OldMain;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.support.ConnectionSource;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.Callable;

public class ImportDataController implements Initializable {
    public TextField pathEdt;
    public Button browsBtn;
    public ProgressBar progressBar;
    public ProgressIndicator progressIndicator;
    public Button loadBtn;
    public Button closeBtn;

    public void onBrowsAction(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Выберите базу данных от Cash Book 1");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Файлы баз данных", "*.db"));
        File file = fileChooser.showOpenDialog(browsBtn.getScene().getWindow());
        if(file != null && file.exists()) {
            pathEdt.setText(file.getAbsolutePath());
        }
    }

    public void onLoadAction(ActionEvent actionEvent) {
        String dbPath = pathEdt.getText();
        if(dbPath == null || dbPath.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Cash Book 2");
            alert.setHeaderText("Введите путь к базе данных от Cash Book 1");
            alert.showAndWait();
            return;
        }

        try {
            ConnectionSource importConnectionSource = ConnectionSourceFactory.getImportConnectionSource(dbPath);
            Dao<OldMain, String> mainDao = DaoManager.createDao(importConnectionSource, OldMain.class);
            Dao<OldDetail, String> detailDao = DaoManager.createDao(importConnectionSource, OldDetail.class);

            ImportDataTask importTask = new ImportDataTask(mainDao, detailDao);
            progressBar.progressProperty().bind(importTask.progressProperty());
            importTask.setOnFailed(event -> {
                progressBar.setVisible(false);
                Alert alert = FXUtils.createExceptionDialog("Cash Book 2", "Ошибка импорта данных из БД", null, event.getSource().getException());
                alert.showAndWait();
                closeImportConnection(importConnectionSource);
                closeBtn.setDisable(false);
            });
            importTask.setOnSucceeded(event -> {
                progressBar.setVisible(false);
                closeImportConnection(importConnectionSource);
                closeBtn.setDisable(false);
                closeBtn.fire();
            });
            importTask.setOnRunning(event -> {
                progressBar.setVisible(true);
            });

            LoadInfoTask loadInfoTask = new LoadInfoTask(mainDao, detailDao);
            loadInfoTask.setOnRunning(event -> {
                progressIndicator.setVisible(true);
                closeBtn.setDisable(true);
            });
            loadInfoTask.setOnFailed(event -> {
                Alert alert = FXUtils.createExceptionDialog("Cash Book 2", "Ошибка получения данных из БД", null, event.getSource().getException());
                alert.showAndWait();
                closeImportConnection(importConnectionSource);
                progressIndicator.setVisible(false);
                closeBtn.setDisable(false);
            });
            loadInfoTask.setOnSucceeded(event -> {
                progressIndicator.setVisible(false);
                Thread t = new Thread(importTask);
                t.start();
            });

            Thread infoT = new Thread(loadInfoTask);
            infoT.start();
        } catch (SQLException e) {
            Alert alert = FXUtils.createExceptionDialog("Cash Book 2", "Ошибка получения данных из БД", null, e);
            alert.showAndWait();
        }
    }

    private void closeImportConnection(ConnectionSource connectionSource) {
        try {
            connectionSource.close();
        } catch (IOException e) {
            Alert alert = FXUtils.createExceptionDialog("Cash Book 2", "Ошибка закрытия соединения с БД", null, e);
            alert.showAndWait();
        }
    }

    public void onCloseAction(ActionEvent actionEvent) {
        ((Stage) closeBtn.getScene().getWindow()).close();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loadBtn.disableProperty().bind(pathEdt.textProperty().isEmpty());
    }

    private class LoadInfoTask extends Task<Long> {
        private final Dao<OldMain, String> mainDao;
        private final Dao<OldDetail, String> detailDao;

        public LoadInfoTask(Dao<OldMain, String> mainDao, Dao<OldDetail, String> detailDao) {
            this.mainDao = mainDao;
            this.detailDao = detailDao;
        }

        @Override
        protected Long call() throws Exception {
            return mainDao.countOf() + detailDao.countOf();
        }
    }

    private class ImportDataTask extends Task<Void> {
        private final Dao<OldMain, String> mainDao;
        private final Dao<OldDetail, String> detailDao;
        private final SimpleDateFormat dateFormat;
        private long max;

        public ImportDataTask(Dao<OldMain, String> mainDao, Dao<OldDetail, String> detailDao) {
            this.mainDao = mainDao;
            this.detailDao = detailDao;
            dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        }

        public long getMax() {
            return max;
        }

        public void setMax(long max) {
            this.max = max;
        }

        @Override
        protected Void call() throws Exception {
            List<OldMain> mains = mainDao.queryForAll();
            List<OldDetail> details = detailDao.queryForAll();
            Dao<CashDay, Date> cashDayDao = DaoFactory.getCashDayDao();
            Dao<CashDayDetail, Long> cashDayDetailDao = DaoFactory.getCashDayDetailDao();
            TransactionManager.callInTransaction(ConnectionSourceFactory.getConnectionSource(), new Callable<Object>() {
                @Override
                public Object call() throws Exception {
                    long current = 0;
                    for(OldMain i : mains) {
                        CashDay cashDay = new CashDay();
                        cashDay.setDate(dateFormat.parse(i.getCashDate()));
                        cashDay.setPrihodCount(i.getPrihodCol());
                        cashDay.setRashodCount(i.getRashodCol());
                        cashDay.setInRest(BigDecimal.valueOf(i.getInRest()).setScale(2, RoundingMode.FLOOR));
                        cashDay.setOutRest(BigDecimal.valueOf(i.getOutRest()).setScale(2, RoundingMode.FLOOR));
                        cashDay.setListNumber(i.getListNum());
                        cashDayDao.create(cashDay);
                        updateProgress(current++, max);
                    }
                    for(OldDetail i : details) {
                        CashDayDetail cashDayDetail = new CashDayDetail(new CashDay(dateFormat.parse(i.getCashDate())));
                        cashDayDetail.setDocumentNumber(i.getDocNum());
                        cashDayDetail.setDocumentDescription(i.getDocInfo());
                        cashDayDetail.setAccountNumber(i.getAccNum());
                        cashDayDetail.setPrihod(BigDecimal.valueOf(i.getPrihod()).setScale(2, RoundingMode.FLOOR));
                        cashDayDetail.setRashod(BigDecimal.valueOf(i.getRashod()).setScale(2, RoundingMode.FLOOR));
                        cashDayDetailDao.create(cashDayDetail);
                        updateProgress(current++, max);
                    }
                    return null;
                }
            });
            return null;
        }
    }
}
