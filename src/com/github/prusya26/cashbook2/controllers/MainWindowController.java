package com.github.prusya26.cashbook2.controllers;

import com.github.prusya26.cashbook2.Main;
import com.github.prusya26.cashbook2.common.AppProperties;
import com.github.prusya26.cashbook2.common.BigDecimalStringConverter;
import com.github.prusya26.cashbook2.common.DateTableCell;
import com.github.prusya26.cashbook2.common.FXUtils;
import com.github.prusya26.cashbook2.controllers.model.CashDayObservable;
import com.github.prusya26.cashbook2.data.ConnectionSourceFactory;
import com.github.prusya26.cashbook2.data.DaoFactory;
import com.github.prusya26.cashbook2.data.ModelFactory;
import com.github.prusya26.cashbook2.data.model.CashDayDetail;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.stmt.DeleteBuilder;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.concurrent.Callable;

public class MainWindowController implements Initializable {

    public Button addBtn;
    public Button editBtn;
    public Button delBtn;
    public Button printBtn;
    public SplitMenuButton settingsBtn;
    public TableView<CashDayObservable> mainTable;
    public TableColumn<CashDayObservable, Date> dateCol;
    public TableColumn<CashDayObservable, BigDecimal> inRestCol;
    public TableColumn<CashDayObservable, BigDecimal> outRestCol;
    public ProgressIndicator progressIndicator;
    public Button designBtn;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        dateCol.setCellValueFactory(param -> param.getValue().dateProperty());
        dateCol.setSortType(TableColumn.SortType.DESCENDING);
        inRestCol.setCellValueFactory(param -> param.getValue().inRestProperty());
        inRestCol.setCellFactory(TextFieldTableCell.forTableColumn(new BigDecimalStringConverter()));
        outRestCol.setCellValueFactory(param -> param.getValue().outRestProperty());
        outRestCol.setCellFactory(TextFieldTableCell.forTableColumn(new BigDecimalStringConverter()));

        dateCol.setCellFactory(param -> new DateTableCell<>());

        loadData();
    }

    private void loadData() {
        DataLoadTask loadTask = new DataLoadTask();
        loadTask.setOnRunning(event -> progressIndicator.setVisible(true));
        loadTask.setOnFailed(event -> {
            progressIndicator.setVisible(false);
            Alert alert = FXUtils.createExceptionDialog("Cash Book 2", "Ошибка чтения данных из БД", "Не удалось получить список дней", event.getSource().getException());
            alert.showAndWait();
        });
        loadTask.setOnSucceeded(event -> {
            mainTable.setItems(loadTask.getValue());
            progressIndicator.setVisible(false);
            mainTable.getSortOrder().add(dateCol);
            mainTable.sort();
        });
        Thread thread = new Thread(loadTask);
        thread.start();
    }

    public void onSettingsAction(ActionEvent actionEvent) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/com/github/prusya26/cashbook2/fxml/settings_window.fxml"));
            Stage stage = new Stage();
            stage.setTitle("Настройки");
            stage.setScene(new Scene(root));
            stage.initOwner(settingsBtn.getScene().getWindow());
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();
        } catch (IOException e) {
            Alert alert = FXUtils.createExceptionDialog("Cash Book 2", "Ошибка открытия окна настроек", null, e);
            alert.showAndWait();
        }
    }

    public void onAddAction(ActionEvent actionEvent) {
        CashDayObservable newDay = ModelFactory.createEmptyCashDayObservable();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/github/prusya26/cashbook2/fxml/detail_window.fxml"));
        try {
            Parent root = loader.load();
            DetailWindowController controller = loader.getController();
            controller.setCashDay(newDay);
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.showAndWait();
            if(controller.isOkPressed()) {
                //Надо добавить кэшдей в таблицу
                mainTable.getItems().add(newDay);
                mainTable.sort();
            }
        } catch (IOException e) {
            Alert alert = FXUtils.createExceptionDialog("Cash Book 2", "Ошибка открытия окна редактирования дня", null, e);
            alert.showAndWait();
        }
    }

    public void onEditAction(ActionEvent actionEvent) {
        CashDayObservable day = mainTable.getSelectionModel().getSelectedItem();
        if(day != null) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/github/prusya26/cashbook2/fxml/detail_window.fxml"));
            try {
                Parent root = loader.load();
                DetailWindowController controller = loader.getController();
                controller.setCashDay(day);
                Stage stage = new Stage();
                stage.setScene(new Scene(root));
                stage.showAndWait();
            } catch (IOException e) {
                Alert alert = FXUtils.createExceptionDialog("Cash Book 2", "Ошибка открытия окна редактирования дня", null, e);
                alert.showAndWait();
            }
        }
    }

    public void onDelAction(ActionEvent actionEvent) {
        CashDayObservable day = mainTable.getSelectionModel().getSelectedItem();
        if(day != null) {
            try {
                TransactionManager.callInTransaction(ConnectionSourceFactory.getConnectionSource(), (Callable<Void>) () -> {
                    Dao<CashDayDetail, Long> detailDao = DaoFactory.getCashDayDetailDao();
                    DeleteBuilder<CashDayDetail, Long> deleteBuilder = detailDao.deleteBuilder();
                    deleteBuilder.where().eq(CashDayDetail.Columns.CASH_DATE, day.getDate());
                    detailDao.delete(deleteBuilder.prepare());
                    DaoFactory.getCashDayDao().delete(day.getSource());
                    return null;
                });
                mainTable.getItems().remove(day);
            } catch (SQLException e) {
                Alert alert = FXUtils.createExceptionDialog("Cash Book 2", "Ошибка удаления дня", null, e);
                alert.showAndWait();
            }
        }
    }

    public void onPrintAction(ActionEvent actionEvent) {
        CashDayObservable day = mainTable.getSelectionModel().getSelectedItem();
        if(day != null) {
            SimpleDateFormat fmt = new SimpleDateFormat("dd.MM.yyyy");
            Main.getJfrRunner().setParam("pdt", fmt.format(day.getDate()));
            Main.getJfrRunner().setParam("cashier", AppProperties.get().getCashier());
            try {
                Main.getJfrRunner().run("report.fr3");
            } catch (IOException e) {
                Alert alert = FXUtils.createExceptionDialog("Cash Book 2", "Ошибка запуска отчета", null, e);
                alert.showAndWait();
            }
        }
    }

    public void onDesignAction(ActionEvent actionEvent) {
        CashDayObservable day = mainTable.getSelectionModel().getSelectedItem();
        if(day != null) {
            SimpleDateFormat fmt = new SimpleDateFormat("dd.MM.yyyy");
            Main.getJfrRunner().setParam("pdt", fmt.format(day.getDate()));
            Main.getJfrRunner().setParam("cashier", AppProperties.get().getCashier());
            try {
                Main.getJfrRunner().design("report.fr3");
            } catch (IOException e) {
                Alert alert = FXUtils.createExceptionDialog("Cash Book 2", "Ошибка запуска дизайнера отчета", null, e);
                alert.showAndWait();
            }
        }
    }

    public void onImportData(ActionEvent actionEvent) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/com/github/prusya26/cashbook2/fxml/import_data_window.fxml"));
            Stage stage = new Stage();
            stage.setTitle("Импорт данных из Cash Book 1");
            stage.setScene(new Scene(root));
            stage.initStyle(StageStyle.UNDECORATED);
            stage.initOwner(settingsBtn.getScene().getWindow());
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();
            progressIndicator.setVisible(true);
            loadData();
        } catch (IOException e) {
            Alert alert = FXUtils.createExceptionDialog("Cash Book 2", "Ошибка открытия окна импорта данных", null, e);
            alert.showAndWait();
        }
    }

    private class DataLoadTask extends Task<ObservableList<CashDayObservable>> {
        @Override
        protected ObservableList<CashDayObservable> call() throws Exception {
            return ModelFactory.getCashDayObservableList();
        }
    }
}
