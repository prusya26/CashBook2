package com.github.prusya26.cashbook2.data.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = CashDayDetailTemplate.TABLE_NAME)
public class CashDayDetailTemplate {
    public static final String TABLE_NAME = "cash_templates";

    public static final class Columns {
        private Columns() {}
        public static final String ID = "id";
        public static final String DOCUMENT_NUMBER = "doc_num";
        public static final String DOCUMENT_DESCRIPTION = "doc_descr";
        public static final String ACCOUNT_NUMBER = "acc_num";
    }

    @DatabaseField(columnName = Columns.ID, generatedId = true)
    private long id;
    @DatabaseField(columnName = Columns.DOCUMENT_NUMBER)
    private String documentNumber;
    @DatabaseField(columnName = Columns.DOCUMENT_DESCRIPTION)
    private String documentDescription;
    @DatabaseField(columnName = Columns.ACCOUNT_NUMBER)
    private String accountNumber;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getDocumentDescription() {
        return documentDescription;
    }

    public void setDocumentDescription(String documentDescription) {
        this.documentDescription = documentDescription;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
}
