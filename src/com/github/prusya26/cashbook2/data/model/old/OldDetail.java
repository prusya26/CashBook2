package com.github.prusya26.cashbook2.data.model.old;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "detail_table")
public class OldDetail {
    @DatabaseField(columnName = "cash_date")
    private String cashDate;
    @DatabaseField(columnName = "doc_num")
    private String docNum;
    @DatabaseField(columnName = "doc_info")
    private String docInfo;
    @DatabaseField(columnName = "acc_num")
    private String accNum;
    @DatabaseField
    private double prihod;
    @DatabaseField
    private double rashod;

    public String getCashDate() {
        return cashDate;
    }

    public void setCashDate(String cashDate) {
        this.cashDate = cashDate;
    }

    public String getDocNum() {
        return docNum;
    }

    public void setDocNum(String docNum) {
        this.docNum = docNum;
    }

    public String getDocInfo() {
        return docInfo;
    }

    public void setDocInfo(String docInfo) {
        this.docInfo = docInfo;
    }

    public String getAccNum() {
        return accNum;
    }

    public void setAccNum(String accNum) {
        this.accNum = accNum;
    }

    public double getPrihod() {
        return prihod;
    }

    public void setPrihod(double prihod) {
        this.prihod = prihod;
    }

    public double getRashod() {
        return rashod;
    }

    public void setRashod(double rashod) {
        this.rashod = rashod;
    }
}
