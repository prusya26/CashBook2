package com.github.prusya26.cashbook2.data.model.old;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "main_table")
public class OldMain {
    @DatabaseField(columnName = "cash_date", id = true)
    private String cashDate;
    @DatabaseField(columnName = "in_rest")
    private double inRest;
    @DatabaseField(columnName = "out_rest")
    private double outRest;
    @DatabaseField(columnName = "list_num")
    private int listNum;
    @DatabaseField(columnName = "prihod_col")
    private String prihodCol;
    @DatabaseField(columnName = "rashod_col")
    private String rashodCol;

    public String getCashDate() {
        return cashDate;
    }

    public void setCashDate(String cashDate) {
        this.cashDate = cashDate;
    }

    public double getInRest() {
        return inRest;
    }

    public void setInRest(double inRest) {
        this.inRest = inRest;
    }

    public double getOutRest() {
        return outRest;
    }

    public void setOutRest(double outRest) {
        this.outRest = outRest;
    }

    public int getListNum() {
        return listNum;
    }

    public void setListNum(int listNum) {
        this.listNum = listNum;
    }

    public String getPrihodCol() {
        return prihodCol;
    }

    public void setPrihodCol(String prihodCol) {
        this.prihodCol = prihodCol;
    }

    public String getRashodCol() {
        return rashodCol;
    }

    public void setRashodCol(String rashodCol) {
        this.rashodCol = rashodCol;
    }
}
