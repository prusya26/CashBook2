package com.github.prusya26.cashbook2.data.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = DbInfo.TABLE_NAME)
public class DbInfo {
    public static final String TABLE_NAME = "db_info";
    public static final String ID = "CashBook2";

    public static class Columns {
        private Columns() {}
        public static final String PROGRAM_NAME = "program_name";
        public static final String VERSION = "ver";
        public static final String CREATE_DATE = "dcreate";
        public static final String UPDATE_DATE = "dupd";
    }

    @DatabaseField(columnName = Columns.PROGRAM_NAME, id = true)
    private String programName;
    @DatabaseField(columnName = Columns.VERSION, canBeNull = false)
    private int version;
    @DatabaseField(columnName = Columns.CREATE_DATE, canBeNull = false)
    private Date createDate;
    @DatabaseField(columnName = Columns.UPDATE_DATE)
    private Date updateDate;

    public DbInfo() {
        programName = ID;
    }

    public String getProgramName() {
        return programName;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
