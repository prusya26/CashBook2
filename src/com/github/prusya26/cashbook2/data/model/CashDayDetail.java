package com.github.prusya26.cashbook2.data.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.math.BigDecimal;

@DatabaseTable(tableName = CashDayDetail.TABLE_NAME)
public class CashDayDetail {
    public static final String TABLE_NAME = "cash_details";

    public static final class Columns {
        private Columns() {}
        public static final String ID = "id";
        public static final String CASH_DATE = "cash_date";
        public static final String DOCUMENT_NUMBER = "doc_num";
        public static final String DOCUMENT_DESCRIPTION = "doc_descr";
        public static final String ACCOUNT_NUMBER = "acc_num";
        public static final String PRIHOD = "prihod";
        public static final String RASHOD = "rashod";
    }

    @DatabaseField(columnName = Columns.ID, generatedId = true)
    private long id;
    @DatabaseField(columnName = Columns.CASH_DATE, foreign = true, canBeNull = false)
    private CashDay cashDay;
    @DatabaseField(columnName = Columns.DOCUMENT_NUMBER)
    private String documentNumber;
    @DatabaseField(columnName = Columns.DOCUMENT_DESCRIPTION)
    private String documentDescription;
    @DatabaseField(columnName = Columns.ACCOUNT_NUMBER)
    private String accountNumber;
    @DatabaseField(columnName = Columns.PRIHOD)
    private BigDecimal prihod;
    @DatabaseField(columnName = Columns.RASHOD)
    private BigDecimal rashod;

    public CashDayDetail() {
    }

    public CashDayDetail(CashDay cashDayColumn) {
        this();
        this.cashDay = cashDayColumn;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public CashDay getCashDay() {
        return cashDay;
    }

    public void setCashDay(CashDay cashDay) {
        this.cashDay = cashDay;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getDocumentDescription() {
        return documentDescription;
    }

    public void setDocumentDescription(String documentDescription) {
        this.documentDescription = documentDescription;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public BigDecimal getPrihod() {
        return prihod;
    }

    public void setPrihod(BigDecimal prihod) {
        this.prihod = prihod;
    }

    public BigDecimal getRashod() {
        return rashod;
    }

    public void setRashod(BigDecimal rashod) {
        this.rashod = rashod;
    }
}
