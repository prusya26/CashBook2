package com.github.prusya26.cashbook2.data.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.math.BigDecimal;
import java.util.Date;

@DatabaseTable(tableName = CashDay.TABLE_NAME)
public class CashDay {
    public static final String TABLE_NAME = "cash_days";

    public static final class Columns {
        private Columns() {
        }

        public static final String CASH_DATE = "cash_date";
        public static final String IN_REST = "in_rest";
        public static final String OUT_REST = "out_rest";
        public static final String LIST_NUMBER = "list_num";
        public static final String PRIHOD_COUNT = "prihod_count";
        public static final String RASHOD_COUNT = "rashod_count";
    }

    @DatabaseField(columnName = Columns.CASH_DATE, id = true, dataType = DataType.DATE_STRING)
    private Date date;
    @DatabaseField(columnName = Columns.IN_REST, dataType = DataType.BIG_DECIMAL)
    private BigDecimal inRest;
    @DatabaseField(columnName = Columns.OUT_REST, dataType = DataType.BIG_DECIMAL)
    private BigDecimal outRest;
    @DatabaseField(columnName = Columns.LIST_NUMBER)
    private int listNumber;
    @DatabaseField(columnName = Columns.PRIHOD_COUNT)
    private String prihodCount;
    @DatabaseField(columnName = Columns.RASHOD_COUNT)
    private String rashodCount;

    public CashDay() {
    }

    public CashDay(Date date) {
        this();
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getInRest() {
        return inRest;
    }

    public void setInRest(BigDecimal inRest) {
        this.inRest = inRest;
    }

    public BigDecimal getOutRest() {
        return outRest;
    }

    public void setOutRest(BigDecimal outRest) {
        this.outRest = outRest;
    }

    public int getListNumber() {
        return listNumber;
    }

    public void setListNumber(int listNumber) {
        this.listNumber = listNumber;
    }

    public String getPrihodCount() {
        return prihodCount;
    }

    public void setPrihodCount(String prihodCount) {
        this.prihodCount = prihodCount;
    }

    public String getRashodCount() {
        return rashodCount;
    }

    public void setRashodCount(String rashodCount) {
        this.rashodCount = rashodCount;
    }
}
