package com.github.prusya26.cashbook2.data;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

public final class ConnectionSourceFactory {
    private static final String DB_URL = "jdbc:sqlite:cashbook2.db";
    private static ConnectionSource connectionSource;

    public static ConnectionSource getConnectionSource() throws SQLException {
        if(connectionSource == null) {
            connectionSource = new JdbcConnectionSource(DB_URL);
        }
        return connectionSource;
    }

    public static ConnectionSource getImportConnectionSource(String dbPath) throws SQLException {
        String dbUrl = "jdbc:sqlite:" + dbPath;
        return new JdbcConnectionSource(dbUrl);
    }
}
