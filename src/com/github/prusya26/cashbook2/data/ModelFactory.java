package com.github.prusya26.cashbook2.data;

import com.github.prusya26.cashbook2.controllers.model.CashDayDetailObservable;
import com.github.prusya26.cashbook2.controllers.model.CashDayDetailTemplateObservable;
import com.github.prusya26.cashbook2.controllers.model.CashDayObservable;
import com.github.prusya26.cashbook2.data.model.CashDay;
import com.github.prusya26.cashbook2.data.model.CashDayDetail;
import com.github.prusya26.cashbook2.data.model.CashDayDetailTemplate;
import com.j256.ormlite.stmt.QueryBuilder;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

public final class ModelFactory {
    public static List<CashDay> getCashDayList(QueryBuilder<CashDay, Date> queryBuilder) throws SQLException {
        List<CashDay> result;
        if(queryBuilder != null) {
            result = DaoFactory.getCashDayDao().query(queryBuilder.prepare());
        } else {
            result = DaoFactory.getCashDayDao().queryForAll();
        }
        return result;
    }

    public static List<CashDay> getCashDayList() throws SQLException {
        return getCashDayList(null);
    }

    public static ObservableList<CashDayObservable> getCashDayObservableList(QueryBuilder<CashDay, Date> queryBuilder) throws SQLException {
        List<CashDay> cashDayList = getCashDayList(queryBuilder);
        ObservableList<CashDayObservable> result = FXCollections.observableArrayList();
        cashDayList.forEach(cashDay -> result.add(new CashDayObservable(cashDay)));
        return result;
    }

    public static ObservableList<CashDayObservable> getCashDayObservableList() throws SQLException {
        return getCashDayObservableList(null);
    }

    public static CashDay createEmptyCashDay() {
        return new CashDay();
    }

    public static CashDayObservable createEmptyCashDayObservable() {
        return new CashDayObservable(createEmptyCashDay());
    }

    public static CashDayDetail createEmptyCashDayDetail() {
        return new CashDayDetail();
    }

    public static CashDayDetailObservable createEmptyCashDayDetailObservable() {
        return new CashDayDetailObservable(createEmptyCashDayDetail());
    }

    public static List<CashDayDetailTemplate> getCashDayDetailTemplateList(QueryBuilder<CashDayDetailTemplate, Long> queryBuilder) throws SQLException {
        List<CashDayDetailTemplate> result;
        if(queryBuilder != null) {
            result = DaoFactory.getCashDayDetailTemplateDao().query(queryBuilder.prepare());
        } else {
            result = DaoFactory.getCashDayDetailTemplateDao().queryForAll();
        }
        return result;
    }

    public static List<CashDayDetailTemplate> getCashDayDetailTemplateList() throws SQLException {
        return getCashDayDetailTemplateList(null);
    }

    public static ObservableList<CashDayDetailTemplateObservable> getCashDayDetailTemplateObservableList(QueryBuilder<CashDayDetailTemplate, Long> queryBuilder) throws SQLException {
        List<CashDayDetailTemplate> cashDayDetailTemplates = getCashDayDetailTemplateList(queryBuilder);
        ObservableList<CashDayDetailTemplateObservable> result = FXCollections.observableArrayList();
        cashDayDetailTemplates.forEach(cashDayDetailTemplate -> result.add(new CashDayDetailTemplateObservable(cashDayDetailTemplate)));
        return result;
    }

    public static ObservableList<CashDayDetailTemplateObservable> getCashDayDetailTemplateObservableList() throws SQLException {
        return getCashDayDetailTemplateObservableList(null);
    }

    public static CashDayDetailTemplate createEmptyCashDayDetailTemplate() {
        return new CashDayDetailTemplate();
    }

    public static CashDayDetailTemplateObservable createEmptyCashDayDetailTemplateObservable() {
        return new CashDayDetailTemplateObservable(createEmptyCashDayDetailTemplate());
    }
}
