package com.github.prusya26.cashbook2.data.update;

import java.sql.SQLException;

public interface Updater {
    void update() throws SQLException;
}
