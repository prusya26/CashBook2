package com.github.prusya26.cashbook2.data;

import com.github.prusya26.cashbook2.data.model.CashDay;
import com.github.prusya26.cashbook2.data.model.CashDayDetail;
import com.github.prusya26.cashbook2.data.model.CashDayDetailTemplate;
import com.github.prusya26.cashbook2.data.model.DbInfo;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.Date;

public class DbHelper {
    private static final int version = 1;

    public void checkDataBase() throws SQLException {
        if(isDataBaseCreated()) {
            int dbInfoVersion = getDbInfoVersion();
            if(dbInfoVersion < version) {
                updateDataBase(dbInfoVersion, version);
            }
        } else {
            createDataBase();
        }
    }

    private boolean isDataBaseCreated() throws SQLException {
        Dao<DbInfo, String> dao = DaoFactory.getDbInfoDao();
        GenericRawResults<String[]> results = dao.queryRaw("select DISTINCT tbl_name from sqlite_master where tbl_name = ?", DbInfo.TABLE_NAME);
        return !results.getResults().isEmpty();
    }

    private void createDataBase() throws SQLException {
        ConnectionSource connectionSource = ConnectionSourceFactory.getConnectionSource();
        TableUtils.createTable(connectionSource, DbInfo.class);
        DbInfo dbInfo = new DbInfo();
        dbInfo.setVersion(version);
        dbInfo.setCreateDate(new Date());
        DaoFactory.getDbInfoDao().create(dbInfo);

        //Остальные таблицы
        TableUtils.createTable(connectionSource, CashDay.class);
        TableUtils.createTable(connectionSource, CashDayDetail.class);
        TableUtils.createTable(connectionSource, CashDayDetailTemplate.class);
    }

    private int getDbInfoVersion() throws SQLException {
        DbInfo dbInfo = DaoFactory.getDbInfoDao().queryForId(DbInfo.ID);
        return dbInfo.getVersion();
    }

    private void updateDataBase(int oldVersion, int newVersion) {

    }
}
