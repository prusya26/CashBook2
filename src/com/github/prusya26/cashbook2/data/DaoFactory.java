package com.github.prusya26.cashbook2.data;

import com.github.prusya26.cashbook2.data.model.CashDay;
import com.github.prusya26.cashbook2.data.model.CashDayDetail;
import com.github.prusya26.cashbook2.data.model.CashDayDetailTemplate;
import com.github.prusya26.cashbook2.data.model.DbInfo;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;

import java.sql.SQLException;
import java.util.Date;

public final class DaoFactory {
    private static Dao<CashDay, Date> cashDayDao;
    private static Dao<CashDayDetail, Long> cashDayDetailDao;
    private static Dao<CashDayDetailTemplate, Long> cashDayDetailTemplateDao;

    public static Dao<CashDay, Date> getCashDayDao() throws SQLException {
        if(cashDayDao == null) {
            cashDayDao = DaoManager.createDao(ConnectionSourceFactory.getConnectionSource(), CashDay.class);
        }
        return cashDayDao;
    }

    public static Dao<CashDayDetail, Long> getCashDayDetailDao() throws SQLException {
        if(cashDayDetailDao == null) {
            cashDayDetailDao = DaoManager.createDao(ConnectionSourceFactory.getConnectionSource(), CashDayDetail.class);
        }
        return cashDayDetailDao;
    }

    public static Dao<DbInfo, String> getDbInfoDao() throws SQLException {
        return DaoManager.createDao(ConnectionSourceFactory.getConnectionSource(), DbInfo.class);
    }

    public static Dao<CashDayDetailTemplate, Long> getCashDayDetailTemplateDao() throws SQLException {
        if(cashDayDetailTemplateDao == null) {
            cashDayDetailTemplateDao = DaoManager.createDao(ConnectionSourceFactory.getConnectionSource(), CashDayDetailTemplate.class);
        }
        return cashDayDetailTemplateDao;
    }
}
