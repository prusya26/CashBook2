package com.github.prusya26.cashbook2.common;

import javafx.util.StringConverter;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class BigDecimalStringConverter extends StringConverter<BigDecimal> {
    private final DecimalFormat format;

    public BigDecimalStringConverter() {
        format = (DecimalFormat) NumberFormat.getNumberInstance(Locale.forLanguageTag("ru"));
        format.setParseBigDecimal(true);
    }

    @Override
    public String toString(BigDecimal object) {
        if(object == null) {
            return null;
        }

        return format.format(object);
    }

    @Override
    public BigDecimal fromString(String string) {
        if(string == null || string.isEmpty()) {
            return null;
        }


        BigDecimal result;
        try {
            result = (BigDecimal) format.parse(string.replace('.', ','));
        } catch (ParseException e) {
            result = new BigDecimal(string);
        }
        return result;
    }
}
