package com.github.prusya26.cashbook2.common;

import javafx.scene.control.TableCell;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTableCell<T> extends TableCell<T, Date> {
    SimpleDateFormat fmt;

    public DateTableCell() {
        fmt = new SimpleDateFormat(AppProperties.get().getDateFormat());
    }

    @Override
    protected void updateItem(Date item, boolean empty) {
        super.updateItem(item, empty);
        if(empty) {
            setText(null);
        } else {
            setText(fmt.format(item));
        }
    }
}
