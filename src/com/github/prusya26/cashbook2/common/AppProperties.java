package com.github.prusya26.cashbook2.common;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public final class AppProperties {
    private static final String KEY_DATE_FORMAT = "dateFormat";
    private static final String KEY_DETAIL_FILL_STRATEGY = "detailFillStrategy";
    private static final String KEY_CASHIER = "cashier";

    private static final String DEFAULT_DATE_FORMAT = "dd.MM.yyyy";
    private static final String DEFAULT_DETAIL_FILL_STRATEGY = DetailFillStrategy.LastFilledDay.name();

    public enum DetailFillStrategy { LastFilledDay, Template }

    private static AppProperties instance;

    private Properties properties;

    private AppProperties() {
        readProperties();
    }

    public static AppProperties get() {
        if(instance == null) instance = new AppProperties();
        return instance;
    }

    private void readProperties() {
        properties = new Properties();
        try {
            properties.loadFromXML(new FileInputStream("config.xml"));
        } catch (IOException e) {
            //Нет у нас конфига, используем значения по умолчанию
        }
    }

    public String getDateFormat() {
        String dateFormat = properties.getProperty(KEY_DATE_FORMAT);
        if(dateFormat == null || dateFormat.isEmpty()) {
            dateFormat = DEFAULT_DATE_FORMAT;
            properties.setProperty(KEY_DATE_FORMAT, dateFormat);
        }
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        properties.setProperty(KEY_DATE_FORMAT, dateFormat);
    }

    public DetailFillStrategy getDetailFillStrategy() {
        String strategyName = properties.getProperty(KEY_DETAIL_FILL_STRATEGY);
        if(strategyName == null || strategyName.isEmpty()) {
            strategyName = DEFAULT_DETAIL_FILL_STRATEGY;
            properties.setProperty(KEY_DETAIL_FILL_STRATEGY, DEFAULT_DETAIL_FILL_STRATEGY);
        }
        return DetailFillStrategy.valueOf(strategyName);
    }

    public void setDetailFillStrategy(DetailFillStrategy strategy) {
        properties.setProperty(KEY_DETAIL_FILL_STRATEGY, strategy.name());
    }

    public String getCashier() {
        String cashier = properties.getProperty(KEY_CASHIER);
        if(cashier == null) cashier = "";
        return cashier;
    }

    public void setCashier(String cashier) {
        properties.setProperty(KEY_CASHIER, cashier);
    }

    public void save() throws IOException {
        properties.storeToXML(new FileOutputStream("config.xml"), null);
    }
}
