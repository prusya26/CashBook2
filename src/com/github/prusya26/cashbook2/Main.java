package com.github.prusya26.cashbook2;

import com.github.prusya26.cashbook2.data.ConnectionSourceFactory;
import com.github.prusya26.cashbook2.data.DbHelper;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ru.inversionkavkaz.jfr.JFRRunner;

import java.sql.SQLException;

public class Main extends Application {

    private static JFRRunner jfrRunner;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("fxml/main_window.fxml"));
        primaryStage.setTitle("Cash Book 2");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
    }

    @Override
    public void stop() throws Exception {
        System.out.println("closing database connection...");
        try {
            ConnectionSourceFactory.getConnectionSource().close();
            System.out.println("database connection closed");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        jfrRunner.close();
        super.stop();
    }

    public static void main(String[] args) {
        System.out.println("checking database...");
        DbHelper dbHelper = new DbHelper();
        try {
            dbHelper.checkDataBase();
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(0);
        }
        System.out.println("database is ok.");
        jfrRunner = new JFRRunner(JFRRunner.Providers.SQLite, "cashbook2.db", null, null, null);
        launch(args);
    }

    public static JFRRunner getJfrRunner() {
        return jfrRunner;
    }
}
